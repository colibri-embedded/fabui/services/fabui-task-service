#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <task_service.hpp>

#include <future>
#include <vector>
#include <memory>
#include <map>
#include <fabui/wamp/mock/session.hpp>
#include <fabui/http/mock/client.hpp>
#include <fabui/wamp/topics.hpp>
#include <fabui/wamp/procedures.hpp>

#include <task/print.hpp>
#include "backend_api.hpp"
#include "topics.hpp"

using namespace fabui;
using namespace std::placeholders;
using namespace std::literals;

class PrintTaskSpy: public PrintTask {
	public:
		PrintTaskSpy(TaskService* service, user_id_t user_id, const FileInfo& file)
			: PrintTask(service, user_id, file)
		{

		}

		TaskStatus getStatus() {
			return FabricationTask::getStatus();
		}

		float getProgress() {
			return FabricationTask::getProgress();
		}

		void changeStatus(TaskStatus status, bool postpone_update=false) {
			return FabricationTask::changeStatus(status, postpone_update);
		}

		void changeProgress(float progress, bool postpone_update=false) {
			return FabricationTask::changeProgress(progress, postpone_update);
		}

		void overrideStatus(TaskStatus status) {
			m_status = status;
		}

		void overrideProgress(float progress) {
			m_progress = progress;
		}
};

class FabuiBackendMock: public HttpClientMock {
	public:
		FabuiBackendMock()
		: HttpClientMock() {
			addResponder(API_TASK_CREATE, [](
					HttpMethod http_method,
					const std::string& sub_url,
					const std::string& data,
					const IHttpClient::params_t& params,
					const IHttpClient::headers_t& headers) -> HttpReponse {

				return HttpReponse{HTTP_CREATED, "{\"id\":1}", ""};
			});
		}
};

/***** TESTS *****/
SCENARIO("New task creation", "[print-task][creation]") {

	WHEN("Create a new task with failed http put") {
		TaskService ts((STORAGE_DIR));
		HttpClientMock http;
		ts.setHttpClient(&http);
        FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};
		PrintTaskSpy *pts;

		THEN("Object is not created") {
			REQUIRE_THROWS_WITH( pts = new PrintTaskSpy(&ts, 1, file_info), "failed to create a new task resource");
		}
	}

	WHEN("Create a new task with successfull http put") {
		TaskService ts(STORAGE_DIR);
		FabuiBackendMock http;
		ts.setHttpClient(&http);
        FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};

		THEN("Object is created") {
			PrintTaskSpy *pts;
			REQUIRE_NOTHROW( pts = new PrintTaskSpy(&ts, 1, file_info), "failed to create a new task resource");
			REQUIRE_NOTHROW( delete pts );
		}
	}

}

SCENARIO("Requirements","[print-task][requirements]") {
    TaskService ts((STORAGE_DIR));
    WampSessionMock wamp;
    ts.setWampSession(&wamp);
    FabuiBackendMock http;
    ts.setHttpClient(&http);
    FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};

    WHEN("requirements is called") {
        PrintTaskSpy pts(&ts, 1, file_info);

        THEN("Corrent requirements are returned") {
            auto req = pts.requirements();
            REQUIRE( req.size() == 2 );
            REQUIRE( req[0] == "head:print" );
            REQUIRE( req[1] == "extruder" );
        }
    }

}

SCENARIO("Print task state","[print-task][read-state]") {
    TaskService ts(STORAGE_DIR);
    FabuiBackendMock http;
    ts.setHttpClient(&http);
    WampSessionMock wamp;
	ts.setWampSession(&wamp);
    FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};
    PrintTaskSpy pts(&ts, 1, file_info);

    WHEN("getState is called") {
        PrintTaskSpy pts(&ts, 1, file_info);

        THEN("state with file info is returned") {
            REQUIRE( pts.start() );
            auto state = pts.getState();

            REQUIRE( state["type"] == "print" );

            REQUIRE( state["file"].is_object() );
            REQUIRE( state["file"]["name"].as_string() == "printable.gcode" );
            REQUIRE( state["file"]["progress"].as_real() == 0.0f );

            REQUIRE( state["print"].is_object() );
            REQUIRE( state["print"]["height"].as_real() == 0.0f );
        }
    }
}

SCENARIO("Run correct macro","[print-task][macros]") {
    TaskService ts(STORAGE_DIR);
    FabuiBackendMock http;
    ts.setHttpClient(&http);
    WampSessionMock wamp;
	ts.setWampSession(&wamp);
    FileInfo file_info{1, "8999bbfda4f0a6206ddea275339a9ec2.gcode", "printable.gcode", "gcode"};
    PrintTaskSpy pts(&ts, 1, file_info);

    wamp.addCallResponder(PROCEDURE_FILE_ABORT, [](wampcc::wamp_args args) -> wampcc::result_info {
        return wampcc::result_info(
                0,      // request_id
                {},     // additional
                wampcc::wamp_args{
                    {true},
                    {}
                },
                nullptr
        );
    });

    WHEN("start is called") {
        auto fut1 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

        THEN("print.start macro is executed") {
            REQUIRE( pts.start() );

            auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
            REQUIRE( wamp.m_called[2].args.args_list[0].as_string() == "print.start");
        }
    }

    WHEN("pause is called") {
        auto fut1 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

        THEN("print.pause macro is executed") {
            pts.overrideStatus(TaskStatus::RUNNING);

            REQUIRE( pts.pause() );
            auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
            REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "print.pause");
        }
    }

    WHEN("resume is called") {
        auto fut1 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

        THEN("print.resume macro is executed") {
            pts.overrideStatus(TaskStatus::PAUSED);

            REQUIRE( pts.resume() );
            auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
            REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "print.resume");
        }
    }

    WHEN("abort is called") {
        auto fut1 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

        THEN("print.abort macro is executed") {
            pts.overrideStatus(TaskStatus::RUNNING);

            REQUIRE( pts.abort() );
            auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
            REQUIRE( wamp.m_called[1].args.args_list[0].as_string() == "print.abort");
        }
    }

    WHEN("finish is called") {
        auto fut1 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

        THEN("print.finish macro is executed") {
            pts.overrideStatus(TaskStatus::RUNNING);

            REQUIRE( pts.finish() );
            auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
            REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "print.finish");
        }
    }

}
