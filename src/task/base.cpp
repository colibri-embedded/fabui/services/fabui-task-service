#include "task/base.hpp"
#include "task_service.hpp"
#include "topics.hpp"
#include "types.hpp"
#include "backend_api.hpp"
#include <fabui/wamp/procedures.hpp>

using namespace fabui;
using namespace wampcc;

TaskBase::TaskBase(TaskService* service, const std::string& type, user_id_t user_id)
 : m_service(service)
 , m_type(type)
 , m_user_id(user_id)
 , m_status(TaskStatus::IDLE)
 , m_progress(0.0f)
{
	auto response = service->getHttpClient()->put(API_TASK_CREATE, json_object{
		{"user_id", user_id},
		{"type", type},
		{"controller", "default"}
	});


	if(response.status_code != HTTP_CREATED) {
		// std::cout << response.json() << std::endl;
		throw std::runtime_error("failed to create a new task resource");
	}

	auto task = response.json();
	m_task_id = task["id"].as_int();

	//std::cout << "TaskBase.TaskId: " << getTaskId() << std::endl << std::flush;
}

TaskBase::~TaskBase()
{

}

void TaskBase::setTaskId(task_id_t task_id) {
	m_task_id = task_id;
}

task_id_t TaskBase::getTaskId() {
	return m_task_id;
}

void TaskBase::setUserId(user_id_t user_id) {
	m_user_id = user_id;
}

user_id_t TaskBase::getUserId() {
	return m_user_id;
}

json_object TaskBase::getState() {
	TaskStatus _status = getStatus();
	std::string status = "IDLE";

	switch(_status) {
		case TaskStatus::RUNNING:
			status = "RUNNING";
			break;
		case TaskStatus::PAUSING:
			status = "PAUSING";
			break;
		case TaskStatus::PAUSED:
			status = "PAUSED";
			break;
		case TaskStatus::RESUMING:
			status = "RESUMING";
			break;
		case TaskStatus::ABORTING:
			status = "ABORTING";
			break;
		case TaskStatus::ABORTED:
			status = "ABORTED";
			break;
		case TaskStatus::FINISHING:
			status = "FINISHING";
			break;
		case TaskStatus::FINISHED:
			status = "FINISHED";
			break;
		default:
			status = "IDLE";
	};

	float progress = getProgress();

	return {
		{"user_id", m_user_id},
		{"task_id", m_task_id},
		{"type", m_type},
		{"status", status},
		{"progress", progress}
	};
}

void TaskBase::notifyStateUpdate() {
	auto state = getState();
	m_service->getWampSession()->publish(TOPIC_TASK, { {TASK_STATE_UPDATE}, state }, {});
}

void TaskBase::changeStatus(TaskStatus status, bool postpone_update) {
	std::unique_lock<std::mutex> guard(m_mutex);
	if(m_status != status) {
		//std::cout << "task.changeStatus: " << int(m_status) << " -> " << int(status) << " \n" << std::flush;
		m_status = status;
		guard.unlock();
		if(!postpone_update)
			notifyStateUpdate();
	}
}

void TaskBase::changeProgress(float progress, bool postpone_update) {
	std::unique_lock<std::mutex> guard(m_mutex);
	if(m_progress != progress) {
		m_progress = progress;
		guard.unlock();
		if(!postpone_update)
			notifyStateUpdate();
	}
}

float TaskBase::getProgress() {
	std::lock_guard<std::mutex> guard(m_mutex);
	return m_progress;
}

TaskStatus TaskBase::getStatus() {
	std::lock_guard<std::mutex> guard(m_mutex);
	return m_status;
}

bool TaskBase::start() {
	if(getStatus() != TaskStatus::IDLE)
		return false;

	auto cfut = m_service->getWampSession()->call(PROCEDURE_FILE_SET_NOTIFY_PARAMS, { {}, {
		{"min_progress", 0.1f},
		{"status_update", true},
		{"height_update", true}
	}}, {});
	cfut.wait();

	//std::cout << m_type + ".start" << " (start)\n";
	cfut = m_service->getWampSession()->call(PROCEDURE_MACRO_RUN, { { m_type + ".start" } }, {});
	cfut.wait();
	//std::cout << m_type + ".start" << " (finish)\n";
	changeStatus(TaskStatus::RUNNING);

	return true;
}

bool TaskBase::pause() {
	if(getStatus() != TaskStatus::RUNNING)
		return false;

	changeStatus(TaskStatus::PAUSING);

	auto cfut = m_service->getWampSession()->call(PROCEDURE_MACRO_RUN, { { m_type + ".pause" } }, {});
	cfut.wait();

	changeStatus(TaskStatus::PAUSED);

	return true;
}

bool TaskBase::resume() {
	if(getStatus() != TaskStatus::PAUSED)
		return false;

    changeStatus(TaskStatus::RESUMING);

	auto cfut = m_service->getWampSession()->call(PROCEDURE_MACRO_RUN, { { m_type + ".resume" } }, {});
	cfut.wait();

    changeStatus(TaskStatus::RUNNING);

	return true;
}

bool TaskBase::abort() {
	// std::cout << "TaskBase::abort\n";
	auto status = getStatus();
	if(	status == TaskStatus::ABORTING  ||
		status == TaskStatus::ABORTED ||
		status == TaskStatus::FINISHING ||
		status == TaskStatus::FINISHED ||
		status == TaskStatus::IDLE )
		return false;

	changeStatus(TaskStatus::ABORTING);

	auto cfut = m_service->getWampSession()->call(PROCEDURE_MACRO_RUN, { { m_type + ".abort" } }, {});
	cfut.wait();

	changeStatus(TaskStatus::ABORTED);

	return true;
}

bool TaskBase::finish() {
    auto status = getStatus();
    if( status != TaskStatus::IDLE &&
        status != TaskStatus::RUNNING &&
        status != TaskStatus::PAUSED)
        return false;

    changeStatus(TaskStatus::FINISHING);

	auto cfut = m_service->getWampSession()->call(PROCEDURE_MACRO_RUN, { { m_type + ".finish" } }, {});
    cfut.wait();

    changeStatus(TaskStatus::FINISHED);

	return true;
}

bool TaskBase::isRunning() {
	auto status = getStatus();
	return (status != TaskStatus::ABORTED &&
			status != TaskStatus::FINISHED &&
            status != TaskStatus::IDLE);
}
