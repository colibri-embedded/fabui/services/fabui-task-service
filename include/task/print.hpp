#ifndef TASK_PRINT_HPP
#define TASK_PRINT_HPP

#include "task/fabrication.hpp"

#define PRINT_TASK "print"

namespace fabui {

class PrintTask : public FabricationTask {
	public:
		PrintTask(TaskService* service, 
			user_id_t user_id,
			const FileInfo& file);

		~PrintTask();

		requirements_t requirements();
		wampcc::json_object getState();
	protected:

};

} // namespace fabui

#endif /* TASK_PRINT_HPP */