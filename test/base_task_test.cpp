#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <task_service.hpp>

#include <future>
#include <vector>
#include <memory>
#include <map>
#include <fabui/wamp/mock/session.hpp>
#include <fabui/http/mock/client.hpp>
#include <fabui/wamp/topics.hpp>
#include <fabui/wamp/procedures.hpp>

#include <task/base.hpp>
#include "backend_api.hpp"
#include "topics.hpp"

using namespace fabui;
using namespace std::placeholders;
using namespace std::literals;

class TaskBaseSpy: public TaskBase {
	public:
		TaskBaseSpy(TaskService* service, const std::string& type, user_id_t user_id)
			: TaskBase(service, type, user_id)
		{

		}

		requirements_t requirements() {
			return {"head:print", "extruder"};
		}

		TaskStatus getStatus() {
			return TaskBase::getStatus();
		}

		float getProgress() {
			return TaskBase::getProgress();
		}

		void changeStatus(TaskStatus status, bool postpone_update=false) {
			return TaskBase::changeStatus(status, postpone_update);
		}

		void changeProgress(float progress, bool postpone_update=false) {
			return TaskBase::changeProgress(progress, postpone_update);
		}

		void overrideStatus(TaskStatus status) {
			m_status = status;
		}

		void overrideProgress(float progress) {
			m_progress = progress;
		}
};

class FabuiBackendMock: public HttpClientMock {
	public:
		FabuiBackendMock()
		: HttpClientMock() {
			addResponder(API_TASK_CREATE, [](
					HttpMethod http_method,
					const std::string& sub_url,
					const std::string& data,
					const IHttpClient::params_t& params,
					const IHttpClient::headers_t& headers) -> HttpReponse {

				return HttpReponse{HTTP_CREATED, "{\"id\":1}", ""};
			});
		}
};

/***** TESTS *****/

SCENARIO("New task creation", "[base-task][creation]") {

	WHEN("Create a new task with failed http put") {
		TaskService ts((STORAGE_DIR));
		HttpClientMock http;
		ts.setHttpClient(&http);

		TaskBaseSpy *tbs;

		THEN("Object is not created") {
			REQUIRE_THROWS_WITH( tbs = new TaskBaseSpy(&ts, "base", 1), "failed to create a new task resource");
		}
	}

	WHEN("Create a new task with successfull http put") {
		TaskService ts(STORAGE_DIR);
		FabuiBackendMock http;
		ts.setHttpClient(&http);

		THEN("Object is created") {
			TaskBaseSpy *tbs;
			REQUIRE_NOTHROW( tbs = new TaskBaseSpy(&ts, "base", 1), "failed to create a new task resource");
			REQUIRE_NOTHROW( delete tbs );
		}
	}

}

SCENARIO("Object getters", "[base-task][getters]") {
	TaskService ts(STORAGE_DIR);
	FabuiBackendMock http;
	ts.setHttpClient(&http);
	TaskBaseSpy tbs(&ts, "base", 101);

	WHEN("getStatus is called") {

	    THEN("Correct status is returned") {
			REQUIRE( tbs.getStatus() == TaskStatus::IDLE );
	    }
	}

	WHEN("getProgress is called") {

	    THEN("Correct progress is returned") {
			REQUIRE( tbs.getProgress() == 0.0f );
	    }
	}

	WHEN("getTaskId is called") {

	    THEN("Correct taskId is returned") {
			REQUIRE( tbs.getTaskId() == 1 );
	    }
	}

	WHEN("getUserId is called") {

	    THEN("Correct userId is returned") {
			REQUIRE( tbs.getUserId() == 101 );
	    }
	}

	WHEN("getState is called") {

	    THEN("Correct state is returned") {
			auto state = tbs.getState();
			REQUIRE( state["type"].as_string() == "base" );
			REQUIRE( state["status"].as_string() == "IDLE" );
			REQUIRE( state["progress"].as_real() == 0.0f );
			REQUIRE( state["user_id"].as_int() == 101 );
			REQUIRE( state["task_id"].as_int() == 1 );
	    }
	}

	WHEN("isRunning is called") {

	    THEN("It returns correct value") {
			tbs.overrideStatus(TaskStatus::IDLE);
			REQUIRE( tbs.isRunning() == false );

			tbs.overrideStatus(TaskStatus::ABORTED);
			REQUIRE( tbs.isRunning() == false );

			tbs.overrideStatus(TaskStatus::FINISHED);
			REQUIRE( tbs.isRunning() == false );

			tbs.overrideStatus(TaskStatus::RUNNING);
			REQUIRE( tbs.isRunning() == true );

			tbs.overrideStatus(TaskStatus::PAUSING);
			REQUIRE( tbs.isRunning() == true );

			tbs.overrideStatus(TaskStatus::PAUSED);
			REQUIRE( tbs.isRunning() == true );

			tbs.overrideStatus(TaskStatus::RESUMING);
			REQUIRE( tbs.isRunning() == true );

			tbs.overrideStatus(TaskStatus::ABORTING);
			REQUIRE( tbs.isRunning() == true );

			tbs.overrideStatus(TaskStatus::FINISHING);
			REQUIRE( tbs.isRunning() == true );
	    }
	}

}

SCENARIO("Object updates", "[base-task][update]") {
	TaskService ts(STORAGE_DIR);
	FabuiBackendMock http;
	WampSessionMock wamp;
	ts.setHttpClient(&http);
	ts.setWampSession(&wamp);
	TaskBaseSpy tbs(&ts, "base", 101);

	// notifyStateUpdate
	WHEN("notifyStateUpdate is called") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 1);

	    THEN("wamp notification is sent") {
			REQUIRE_NOTHROW( tbs.notifyStateUpdate() );

			auto status = fut1.wait_for( std::chrono::seconds(2) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
	    }
	}
	// changeStatus
	WHEN("changeStatus is called") {
	    auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 1);

	    THEN("wamp nofitication is send with correct state") {
	        tbs.changeStatus(TaskStatus::RUNNING);

			auto status = fut1.wait_for( std::chrono::seconds(2) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);

			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "RUNNING" );
	    }
	}

	WHEN("changeStatus is called with the same status") {
	    auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 1);

	    THEN("wamp nofitication is not sent") {
			tbs.overrideStatus(TaskStatus::RUNNING);
	        tbs.changeStatus(TaskStatus::RUNNING);

			auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::timeout );
	    }
	}

	WHEN("changeStatus is called with postpone_update") {
	    auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 1);

	    THEN("wamp nofitication is not sent") {
			tbs.overrideStatus(TaskStatus::RUNNING);
	        tbs.changeStatus(TaskStatus::ABORTING, true);

			auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::timeout );
	    }
	}
	// changeProgress
	WHEN("changeProgress is called") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 1);

	    THEN("wamp nofitication is sent") {
			tbs.changeProgress(50.0f);

			auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);

			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["progress"].as_real() == 50.0f );
	    }
	}

	WHEN("changeProgress is called with same progress") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 1);

	    THEN("wamp nofitication is sent") {
			tbs.overrideProgress(50.0f);
			tbs.changeProgress(50.0f);

			auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::timeout );
	    }
	}

	WHEN("changeProgress is called with postpone_update") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 1);

	    THEN("wamp nofitication is sent") {
			tbs.overrideProgress(0.0f);
			tbs.changeProgress(50.0f, true);

			auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::timeout );
	    }
	}

}

SCENARIO("Object state transition", "[base-task][states]") {
	TaskService ts(STORAGE_DIR);
	FabuiBackendMock http;
	WampSessionMock wamp;
	ts.setHttpClient(&http);
	ts.setWampSession(&wamp);
	TaskBaseSpy tbs(&ts, "base", 101);

	// start
	WHEN("start is called") {
		auto fut1 = wamp.getCallMatchFuture(PROCEDURE_FILE_SET_NOTIFY_PARAMS);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("state is changed to RUNNING") {
	        REQUIRE( tbs.start() );

			auto status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

			status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );

			REQUIRE( tbs.isRunning() );
			REQUIRE( wamp.m_called[1].args.args_list[0].as_string() == "base.start");
	    }
	}

	WHEN("start is called on not IDLE task") {

	    THEN("start returns false") {
			tbs.overrideStatus(TaskStatus::RUNNING);
	        REQUIRE_FALSE( tbs.start() );

			tbs.overrideStatus(TaskStatus::PAUSING);
	        REQUIRE_FALSE( tbs.start() );

			tbs.overrideStatus(TaskStatus::RESUMING);
	        REQUIRE_FALSE( tbs.start() );

			tbs.overrideStatus(TaskStatus::ABORTING);
	        REQUIRE_FALSE( tbs.start() );

			tbs.overrideStatus(TaskStatus::FINISHING);
	        REQUIRE_FALSE( tbs.start() );

			tbs.overrideStatus(TaskStatus::ABORTED);
	        REQUIRE_FALSE( tbs.start() );

			tbs.overrideStatus(TaskStatus::PAUSED);
	        REQUIRE_FALSE( tbs.start() );
	    }
	}

	// pause
	WHEN("pause is called on a started task") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("task is paused") {
			tbs.overrideStatus(TaskStatus::RUNNING);
	        REQUIRE( tbs.pause() );

			auto status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "base.pause");

			status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "PAUSING" );
			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			state = wamp.m_published[1].args.args_dict;
			REQUIRE( state["status"] == "PAUSED" );
	    }
	}

	WHEN("pause is called on a not stated task") {

	    THEN("pause returns false") {
	        tbs.overrideStatus(TaskStatus::IDLE);
			REQUIRE_FALSE( tbs.pause() );
	    }
	}

	// resume
	WHEN("resume is called on a paused task") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("task is resumed") {
			tbs.overrideStatus(TaskStatus::PAUSED);
			REQUIRE( tbs.resume() );

			auto status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "base.resume");

			status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "RESUMING" );
			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			state = wamp.m_published[1].args.args_dict;
			REQUIRE( state["status"] == "RUNNING" );
	    }
	}

	WHEN("resume is called on a not paused task") {

	    THEN("resume returns false") {
			tbs.overrideStatus(TaskStatus::IDLE);
			REQUIRE_FALSE( tbs.resume() );
	    }
	}

	// abort
	WHEN("abort is called on an idle task") {

	    THEN("abort returns false") {
			tbs.overrideStatus(TaskStatus::IDLE);
			REQUIRE_FALSE( tbs.abort() );
	    }
	}

	WHEN("abort is called on a started task") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("task is aborted") {
			tbs.overrideStatus(TaskStatus::RUNNING);
			REQUIRE( tbs.abort() );

			auto status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "base.abort");

			status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "ABORTING" );
			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			state = wamp.m_published[1].args.args_dict;
			REQUIRE( state["status"] == "ABORTED" );
	    }
	}

	WHEN("abort is called on a paused task") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("task is aborted") {
			tbs.overrideStatus(TaskStatus::PAUSED);
			REQUIRE( tbs.abort() );

			auto status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "base.abort");

			status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "ABORTING" );
			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			state = wamp.m_published[1].args.args_dict;
			REQUIRE( state["status"] == "ABORTED" );
	    }
	}

	WHEN("abort is called on a aborted task") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("abort returns false") {
			tbs.overrideStatus(TaskStatus::ABORTED);
			REQUIRE_FALSE( tbs.abort() );
	    }
	}

	WHEN("abort is called on a finished task") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("abort returns false") {
			tbs.overrideStatus(TaskStatus::FINISHED);
			REQUIRE_FALSE( tbs.abort() );
	    }
	}

	// finish
	WHEN("finish is called on a started task") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_TASK, 2);
		auto fut2 = wamp.getCallMatchFuture(PROCEDURE_MACRO_RUN);

	    THEN("task is finished") {
			tbs.overrideStatus(TaskStatus::RUNNING);
			REQUIRE( tbs.finish() );

			auto status = fut2.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_called[0].args.args_list[0].as_string() == "base.finish");

			status = fut1.wait_for( std::chrono::seconds(1) );
			REQUIRE( status == std::future_status::ready );
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			auto state = wamp.m_published[0].args.args_dict;
			REQUIRE( state["status"] == "FINISHING" );
			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == TASK_STATE_UPDATE);
			state = wamp.m_published[1].args.args_dict;
			REQUIRE( state["status"] == "FINISHED" );
	    }
	}

	WHEN("finish is called on an finishing") {

	    THEN("finish returns false") {
			tbs.overrideStatus(TaskStatus::FINISHING);
			REQUIRE_FALSE( tbs.finish() );
	    }
	}

	WHEN("finish is called on an finished") {

	    THEN("finish returns false") {
			tbs.overrideStatus(TaskStatus::FINISHED);
			REQUIRE_FALSE( tbs.finish() );
	    }
	}

	WHEN("finish is called on an aborting") {

	    THEN("finish ") {
			tbs.overrideStatus(TaskStatus::ABORTING);
			REQUIRE_FALSE( tbs.finish() );
	    }
	}

	WHEN("finish is called on an aborted") {

	    THEN("finish ") {
			tbs.overrideStatus(TaskStatus::ABORTED);
			REQUIRE_FALSE( tbs.finish() );
	    }
	}

	// finish on paused?
	// finish on pausing?
	// finish on resuming?
}
