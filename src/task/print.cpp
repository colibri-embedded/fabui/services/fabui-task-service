#include "task/print.hpp"
#include "task_service.hpp"
#include "types.hpp"

using namespace fabui;
using namespace wampcc;

PrintTask::PrintTask(
	TaskService* service,
	user_id_t user_id,
	const FileInfo& file)
	: FabricationTask(service, PRINT_TASK, user_id, file)
{
	//std::cout << "PrintTask.TaskId: " << getTaskId() << std::endl << std::flush;
}

PrintTask::~PrintTask()
{

}

requirements_t PrintTask::requirements() {
	return {
		"head:print",
		"extruder"
	};
}

json_object PrintTask::getState() {
	auto state = FabricationTask::getState();

	json_object print = {
		{ "height", 0.0f }
	};

	state["print"] = std::move(print);

	return state;
}
