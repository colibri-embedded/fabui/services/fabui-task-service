#include <iostream>
#include <fabui/utils/string.hpp>
#include <fabui/utils/os.hpp>

#include "config.h"
#include "task_service.hpp"
#include "task/print.hpp"
#include "backend_api.hpp"

#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

TaskService::TaskService(const std::string& storage_path)
	: m_http(nullptr)
	, m_storage_path(storage_path)
{
	//m_file_state = FileState::NONE;
}

TaskService::~TaskService() {

}

void TaskService::setHttpClient(IHttpClient *http_client)
{
	m_http = http_client;
}

IHttpClient* TaskService::getHttpClient()
{
	assert( m_http != nullptr );
	return m_http;
}

void TaskService::setWampSession(IWampSession* session)
{
	m_session = session;
}

IWampSession* TaskService::getWampSession()
{
	return m_session;
}

bool TaskService::start(user_id_t user_id, const std::string& type, const std::string& controller, const std::string& file_uri)
{
	// check file
	std::cout << "Creating a new task...\n";
	auto uri = wampcc::uri_parts::parse(file_uri);
	std::string fileId = uri.path.substr(1);
	std::string file_url = fmt::format(API_FILE_GET, uri.domain, fileId);

	auto response = m_http->get(file_url);
	if(response.status_code != HTTP_OK)
		throw std::runtime_error("cannot get file info");

	auto backend_file_info = response.json();
	//std::string filename = file_info["filename"].as_string();



	if(type == PRINT_TASK) {
		//
		unsigned fileId = backend_file_info["id"].as_int();
		std::string filename = utils::os::pathJoin( {m_storage_path, backend_file_info["filename"].as_string()} );
		std::string ext = backend_file_info["ext"].as_string();
		std::string name = backend_file_info["name"].as_string() + ext;
		FileInfo file_info{fileId, filename, name, ext};
		std::cout << "PrintTask..." << filename << "\n";
		std::lock_guard<std::mutex> guard(m_task_mut);

		currentTask = std::make_unique<PrintTask>(this, user_id, file_info);
	} else {
		throw std::runtime_error(fmt::format("Task type \"{0}\" is not supported", type));
	}

	/* check requirements */
	auto requirements = currentTask->requirements();
	// TODO: check requirements with machine capabilities

	std::lock_guard<std::mutex> guard(m_task_mut);
	currentTask->start();

	std::cout << "Task started...\n";

	return true;
}

bool TaskService::abort() {
	std::lock_guard<std::mutex> guard(m_task_mut);
	if(not (bool)currentTask) {
		std::cout << "Abort: no running task\n";
		return false;
	}
	return currentTask->abort();
}

bool TaskService::pause() {
	std::lock_guard<std::mutex> guard(m_task_mut);
	if(not (bool)currentTask)
		return false;
	return currentTask->pause();
}

bool TaskService::resume() {
	std::lock_guard<std::mutex> guard(m_task_mut);
	if(not (bool)currentTask)
		return false;
	return currentTask->resume();
}

json_object TaskService::info() {
	std::lock_guard<std::mutex> guard(m_task_mut);
	if(not (bool)currentTask)
		return {
			{"status", "IDLE"},
			{"type", ""},
			{"controller", ""}
		};

	auto state = currentTask->getState();
	return state;
}

/*void TaskService::runnerThreadLoop() {
	std::cout << "runnerThread started\n";

	bool running = true;
	while(true) {
		std::unique_lock<std::mutex> guard(m_running_mut);
		running = m_running;
		//terminated = m_terminated;
		guard.unlock();

		if(!running)
			return;

		e_start.wait();

		if((bool)currentTask) {

			currentTask->start();

			while(currentTask->isRunning()) {

			}

			// currentTask->finish();

		}

		// e_finish.wait();

	}
}*/
