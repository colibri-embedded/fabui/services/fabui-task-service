#ifndef BACKEND_API_HPP
#define BACKEND_API_HPP

#define API_FILE_GET "/v1/storages/{0}/files/{1}"

#define API_TASK_CREATE "/v1/tasks"
#define API_TASK_GET    "/v1/tasks/{0}"
#define API_TASK_UPDATE "/v1/tasks/{0}"
#define API_TASK_DELETE "/v1/tasks/{0}"

#endif /* BACKEND_API_HPP */