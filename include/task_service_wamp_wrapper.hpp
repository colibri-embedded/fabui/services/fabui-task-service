/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: task_service_wamp_wrapper.hpp
 * @brief TaskService wamp wrapper definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef TASK_SERVICE_WAMP_WRAPPER_HPP
#define TASK_SERVICE_WAMP_WRAPPER_HPP

#include "task_service.hpp"
#include <fabui/wamp/backend_service.hpp>

namespace fabui {

class TaskServiceWrapper: public BackendService {
	public:
		TaskServiceWrapper(const std::string& env_file, TaskService& service);

	protected:
		TaskService& m_service;

		/* backend-service */
		void onConnect();
		void onDisconnect();
		void onJoin();

		/* Service functions */
		void terminate_wrapper(wampcc::invocation_info info);

		/* Task functions */
		void start_wrapper(wampcc::invocation_info info);
		void abort_wrapper(wampcc::invocation_info info);
		void pause_wrapper(wampcc::invocation_info info);
		void resume_wrapper(wampcc::invocation_info info);
		void info_wrapper(wampcc::invocation_info info);

};

} // namespace fabui

#endif /* TASK_SERVICE_WAMP_WRAPPER_HPP */